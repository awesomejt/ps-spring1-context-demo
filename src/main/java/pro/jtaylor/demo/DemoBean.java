package pro.jtaylor.demo;

public class DemoBean {
	private String name = null;
	private int length = 0;
	
	public DemoBean() {
		
	}
	
	public DemoBean(String name, int length) {
		super();
		this.name = name;
		this.length = length;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getLength() {
		return length;
	}

	public void setLength(int length) {
		this.length = length;
	}
}
