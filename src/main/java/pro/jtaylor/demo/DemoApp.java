package pro.jtaylor.demo;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class DemoApp {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("context.xml");
		DemoBean bean = context.getBean(DemoBean.class);
		System.out.println("Name : " + bean.getName());
		System.out.println("Length : " + bean.getLength());
	}

}
